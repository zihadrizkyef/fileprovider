package com.zhd.testfileprovider

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.zhd.testfileprovider.helper.BitmapToFile
import com.zhd.testfileprovider.helper.ProfileViewGenerator
import com.zhd.testfileprovider.helper.ViewToBitmap


class MainActivity : AppCompatActivity() {
    private lateinit var buttonRender: Button
    private lateinit var editName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonRender = findViewById(R.id.buttonRender)
        editName = findViewById(R.id.editName)

        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                if (allPermissionGranted()) {
                    process()
                }
            }


        buttonRender.setOnClickListener {
            if (allPermissionGranted()) {
                process()
            } else {
                requestPermissionLauncher.launch(
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                )
            }
        }
    }

    private fun allPermissionGranted(): Boolean {
        val canWrite = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        val canRead = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        return canWrite && canRead
    }

    private fun process() {
        val dialog = ProgressDialog.show(this, "Rendering image", "Tunggu sis...", true)

        val name = editName.text.toString()

        Thread {
            try {
                val view = ProfileViewGenerator.makeView(
                    this,
                    name,
                    "Hidup bermanfaat oye oye",
                    "https://picsum.photos/200"
                )
                val bitmap = ViewToBitmap.convert(view, 1080, 1200)
                val uri = BitmapToFile.write(this, bitmap, name)

                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_STREAM, uri)
                intent.putExtra(Intent.EXTRA_TEXT, "shareText")
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            dialog.dismiss()
        }.start()
    }
}