package com.zhd.testfileprovider

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 11/12/20.
 *
 * Awalnya kita testing di android 10, tanpa :
 *   1. file provider_paths.xml
 *   2. FileProvider.getUri(...)
 *   3. Runtime Ask Permission
 *
 * Di hp tersebut lancar ok jaya. Tapi masih heran knp banyak error terkait permission, maka kita
 * test di hp lain dan benar terjadi error di android 9 kebawah. Dan errornya pun semua list diatas
 * required. Bisa error access denied, bisa error "exposed beyond ClipData....", dll.
 *
 * Maka intinya, kalo bikin app yg berurusan dengan file writing or reading, 3 point diatas wajib
 * diimplement supaya support semua versi OS
 */
