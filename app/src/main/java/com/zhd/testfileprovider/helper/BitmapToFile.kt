package com.zhd.testfileprovider.helper

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import androidx.core.net.toUri
import java.io.File
import java.io.FileOutputStream

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 15/09/20.
 */
object BitmapToFile {
    private const val directoryName = "MyNewApp"

    fun write(context: Context, bitmap: Bitmap, fileName: String): Uri? {
        val newFileName = prepareFileNameExtension(fileName)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            writeAndroidQ(context, bitmap, newFileName)
        } else {
            writeBelowAndroidQ(context, bitmap, newFileName)
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun writeAndroidQ(context: Context, bitmap: Bitmap, fileName: String): Uri? {
        val resolver = context.contentResolver
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
            put(
                MediaStore.MediaColumns.RELATIVE_PATH,
                Environment.DIRECTORY_PICTURES + "/$directoryName"
            )
        }
        val uri = resolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            contentValues
        )

        uri?.let {
            resolver.openOutputStream(uri).use {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
            }
        }

        return uri
    }

    private fun writeBelowAndroidQ(context: Context, bitmap: Bitmap, fileName: String): Uri? {
        try {
            val pathDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                    .toString() + "/$directoryName/"
            )
            pathDir.mkdirs()

            val imageFile = File(pathDir, fileName)
            FileOutputStream(imageFile).use {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
            }

            return FileProvider
                .getUriForFile(
                    context,
                    "${context.applicationContext.packageName}.file_provider",
                    imageFile
                )
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    /**
     * Remove the extension (if exists). And add proper extension
     */
    private fun prepareFileNameExtension(fileName: String): String {
        val pos = fileName.lastIndexOf(".")
        val newFileName = if (pos == -1) fileName else fileName.substring(0, pos)
        return "$newFileName.png"
    }
}
