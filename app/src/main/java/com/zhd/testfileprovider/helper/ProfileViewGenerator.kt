package com.zhd.testfileprovider.helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.zhd.testfileprovider.R
import java.net.URL


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 15/09/20.
 */
object ProfileViewGenerator {

    /**
     * Generate view with ProfileShareData as the data to show in the generated view
     */
    @SuppressLint("InflateParams")
    fun makeView(context: Context, name: String, motoHidup: String, imageUrl: String): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.content_share, null)
        view.findViewById<TextView>(R.id.textName).text = name
        view.findViewById<TextView>(R.id.textMotoHidup).text = motoHidup

        val url = URL(imageUrl)
        val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        view.findViewById<ImageView>(R.id.imageView).setImageBitmap(bmp)

        return view
    }
}