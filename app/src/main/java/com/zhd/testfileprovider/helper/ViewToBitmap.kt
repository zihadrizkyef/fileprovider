package com.zhd.testfileprovider.helper

import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.View

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 15/09/20.
 */
object ViewToBitmap {
    /**
     * Convert a view to bitmap, with specific bitmap width and height
     */
    fun convert(view: View, bitmapWidth: Int, bitmapHeight: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        view.measure(
            View.MeasureSpec.makeMeasureSpec(bitmapWidth, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(bitmapHeight, View.MeasureSpec.EXACTLY)
        )
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        view.draw(canvas)

        return bitmap
    }
}